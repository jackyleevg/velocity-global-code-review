const _ = require('lodash');

let conditions = new Map()
conditions.set("in", (op1, op2) => { return ((op2 || []).includes((op1 || false))); })
conditions.set("not in", (op1, op2) => { return !((op2 || []).includes(op1 || false)); })
conditions.set("any", (op1, op2) => { return (op1 || []).some(r => (op2 || []).includes(r)); })
conditions.set("all", (op1, op2) => { return (op1 ? op1.every(r => (op2 || []).includes(r)) : false) })
conditions.set("equals", (op1, op2) => { return (_.isEqual(op1, op2)); })
conditions.set("not equals", (op1, op2) => { return !(_.isEqual(op1, op2)); })
conditions.set("exists", (op1) => { return (!(_.isEmpty(op1) || _.isNull(op1) || _.isNaN(op1) || _.isUndefined(op1)) || (_.isBoolean(op1) || _.isNumber(op1))) })
conditions.set("not exists", (op1) => { return ((_.isEmpty(op1) || _.isNull(op1) || _.isNaN(op1) || _.isUndefined(op1)) && !(_.isBoolean(op1) || _.isNumber(op1))) })

class DataCondition {
    constructor(propertyName, operator, value) {
        if (!(conditions.get(operator))) {
            throw `Operator ${operator} is not supported. Use ${conditions.keys()}`
        }
        this.propertyName = propertyName
        this.operator = operator
        this.value = value
    }
    getValue(entity) {
        logger.debug("Checking property %s in object: %s",
            this.propertyName, JSON.stringify(entity))
        let keys = this.propertyName.split(".")
        let nObj = entity
        for (let key of keys) {
            try {
                if (key in nObj) {
                    nObj = nObj[key]
                } else {
                    return null
                }
            }
            catch (ex) {
                return null
            }

        }
        return nObj
    }
    meetsCondition(entity) {
        let value = this.getValue(entity)
        let conditionFunc = conditions.get(this.operator)
        return conditionFunc(value, this.value)
    }
}
